# -*- coding: utf-8 -*-
"""
Omikuji.py
CREATED:2019-01-26
DESCRIPTION:
    おみくじプログラム
    起動すると、IDmとPMmの値を利用して、おみくじの結果を出力します。
    結果の出力は、画面への文字の出力と、対応する効果音です。
    Ctl+Zで強制終了されるまで、ICカードの読み取りを繰り返し行います。
"""
import binascii
import nfc
import nfc.tag.tt3, nfc.tag.tt4
import pygame.mixer
import os
import time
import logging


# ログ表示の設定
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# 実行パスの取得
running = os.path.dirname(os.path.abspath(__name__))
# 効果音を相対パスで取得
joined_path = os.path.join(running, '../SE')
se_path = os.path.normpath(joined_path)

# 変数・定数定義(追加削除可能)
OMIKUJI = [(u"大吉", 'pro.mp3'),
           (u"中吉", 'trumpet.mp3'),
           (u"小吉", 'correct.mp3'),
           (u"吉", 'stupid.mp3'),
           (u"末吉", 'flee.mp3'),
           (u"凶", 'incorrect.mp3'),
           (u"大凶", 'fate.mp3')]


def connected(tag):
    """
    タグ情報の読み取りを行う関数
    :param tag:ICカード読み取り結果情報
    :return:読み取り結果
    """
    # TagTypeの出力
    print "[TagType]:", tag.type

    return


def tag_analysis(tag):
    """
    読み取ったカードデータから、おみくじ判定に必要な情報を抽出する関数
    :param tag:ICカード読み取り結果情報
    :return:抽出した情報
    """
    # 結果返却用の関数
    result = []

    if isinstance(tag, nfc.tag.tt3.Type3Tag):
        # Type3Tagの場合
        result.append(binascii.hexlify(tag.idm).upper())
        result.append(binascii.hexlify(tag.pmm).upper())
    elif isinstance(tag, nfc.tag.tt4.Type4Tag):
        # Type4Tagの場合
        result.append(binascii.hexlify(tag.identifier).upper())

    return result


def drawFortune(dataSet):
    """
    カードデータから、おみくじ結果を出力するために計算を行う関数
    :param dataSet:connected関数の実行結果
    :return:おみくじ結果に対応する数値
    """
    # ICカードのデータから、整数を作成
    tmp = 0
    for x in dataSet:
        tmp += int(x, 16)

    # 最終結果から、おみくじの種類に応じた数で割った余りを返却する
    return tmp % len(OMIKUJI)


def showResult(number):
    """
    おみくじの結果出力を行う関数
    :param number:おみくじ結果
    """
    # コンソールへの出力
    print u"あなたの運勢は [", OMIKUJI[number][0], u"] です！"
    # mixerモジュールの初期化
    pygame.mixer.init()
    # 音楽ファイルの読み込み
    pygame.mixer.music.load(se_path + '/' + OMIKUJI[number][1])
    pygame.mixer.music.play(1)



# 結果出力のテストコード
#for x in range(0, len(OMIKUJI), 1):
#    showResult(x)
#    time.sleep(10)


# Ctl+Zが入力されるまでは繰り返し実行し続ける。
while True:
    # カードデータの読み取り
    clf = nfc.ContactlessFrontend('usb')
    print "カードをかざしてください(Ctl+Zで終了)"
    read_tag = clf.connect(rdwr={'on-connect': connected})
    # デバイスを開きっぱなしだと二回目以降に例外が発生するため、クローズしておく
    clf.close()
    data = tag_analysis(read_tag)

    # カード情報から、おみくじデータを作成
    fortune_number = drawFortune(data)

    # おみくじデータから結果を出力
    showResult(fortune_number)

    # 3秒待機してから繰り返し
    time.sleep(3)
