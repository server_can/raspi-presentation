# -*- coding: utf-8 -*-
"""
nfcTest.py
CREATED:2019-01-26
DESCRIPTION:
    nfcPyを利用する前の動作確認用テスト
    1.ID(IDm)とPMMの取得
    2.ランダムアクセス用のパラメータを取得
"""

import nfc
import nfc.tag.tt1, nfc.tag.tt2, nfc.tag.tt3, nfc.tag.tt4
import binascii
import logging

def connected(tag):
    """
    1.タグタイプを読み取り、IDmやらPMmやらを取得する関数
    :param tag:
    :return:
    """
    # TagTypeの出力
    print "TagType:",tag.type
    # Tagの中身を出力
    if isinstance(tag, nfc.tag.tt3.Type3Tag):
        # Type3Tagの場合(社員証はこれ)
        print "[IDm]:", binascii.hexlify(tag.idm).upper()
        print "[PMm]:", binascii.hexlify(tag.pmm).upper()
        print "[Dump]:", tag.dump()
    elif isinstance(tag, nfc.tag.tt4.Type4Tag):
        # Type4Tagの場合(スマホのNFCはこれ)
        print "[UID]:", binascii.hexlify(tag.identifier).upper()
        print "[Dump]:", tag.dump()

    else:
        # Type1/2はとりあえず未対応カードとして取り扱います
        print "未対応のタグタイプです..."


def connected_proof(tag):
    """
    2.システムコード、サービスコード、ブロックコードを指定する
    社員情報が格納されているブロック情報は以下
    システムコード：835F
    サービスコード：64
    ブロックコード：0000,0001
    :param tag:
    :return:
    """
    system_code = 0x835F
    service_code = 64
    sc = nfc.tag.tt3.ServiceCode(service_code, 0x0b)
    bc1 = nfc.tag.tt3.BlockCode(0, service_code)
    bc2 = nfc.tag.tt3.BlockCode(1, service_code)
    # 共通のカードを利用している想定で、とりえず対応カードの選別
    if not isinstance(tag, nfc.tag.tt3.Type3Tag):
        print "未対応のカードです"
        return

    try:
        print tag.polling(system_code)
        data1 = tag.read_without_encryption([sc], [bc1])
        data2 = tag.read_without_encryption([sc], [bc2])
        dataSet = {"block0000":data1, "block0001":data2}
        print dataSet

    except Exception as ex:
        print "エラーが発生しました"
        print ex.message

    return dataSet


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

clf = nfc.ContactlessFrontend('usb')
clf.connect(rdwr={'on-connect': connected_proof})
